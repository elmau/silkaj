stages:
  - checks
  - tests
  - package
  - coverage

variables:
  DOCKER_IMAGE: "registry.duniter.org/docker/python3/poetry"
  PYTHON_VERSION: "3.9"

image: $DOCKER_IMAGE/$PYTHON_VERSION:latest

.code_changes:
  rules:
    - changes:
      - silkaj/*.py
      - tests/*.py

.changes:
  rules:
    - changes:
      - silkaj/*.py
      - tests/*.py
      - .gitlab-ci.yml
      - pyproject.toml
      - poetry.lock

build:
  extends: .changes
  stage: checks
  script:
    - poetry build

.pre-commit:
  variables:
    PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit
  cache:
    paths:
      - ${PRE_COMMIT_HOME}

flake8:
  extends:
    - .code_changes
    - .pre-commit
  stage: checks
  script:
    - pre-commit run -a flake8

format:
  extends:
    - .code_changes
    - .pre-commit
  stage: checks
  script:
    - pre-commit run -a black

isort:
  extends:
    - .code_changes
    - .pre-commit
  stage: checks
  script:
    - pre-commit run -a isort

mypy:
  extends:
    - .code_changes
    - .pre-commit
  stage: checks
  script:
    - pre-commit run -a mypy

pyupgrade:
  extends:
    - .code_changes
    - .pre-commit
  stage: checks
  script:
    - pre-commit run -a pyupgrade

pre-commit:hooks:
  extends:
    - .pre-commit
  stage: checks
  script:
    - pre-commit run -a check-ast
    - pre-commit run -a check-merge-conflict
    - pre-commit run -a check-toml
    - pre-commit run -a debug-statements
    - pre-commit run -a end-of-file-fixer
    - pre-commit run -a mixed-line-ending
    - pre-commit run -a trailing-whitespace
    - pre-commit run -a insert-license
    - pre-commit run -a mdformat

.tests:
  extends: .changes
  stage: tests
  image: $DOCKER_IMAGE/$PYTHON_VERSION:latest
  script:
    - poetry install
    - poetry run pytest

tests:3.7:
  extends: .tests
  tags: [mille]
  variables:
    PYTHON_VERSION: "3.7"

tests:3.8:
  extends: .tests
  tags: [redshift]
  variables:
    PYTHON_VERSION: "3.8"

tests:3.9:coverage:
  extends: .tests
  tags: [redshift]
  script:
    - poetry install
    - poetry run pytest --cov silkaj --cov-report html:cov_html --cov-report term
  artifacts:
    paths:
      - cov_html
    expire_in: 2 days

tests:3.10:
  extends: .tests
  tags: [redshift]
  variables:
    PYTHON_VERSION: "3.10"

.image:
  stage: package
  tags: [docker]
  image: docker:latest
  services:
    - docker:dind
  variables:
    PYTHON_VERSION: "3.10"
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker build --pull -t "$CI_REGISTRY_IMAGE/$CHANNEL/$ENV:$CI_COMMIT_SHORT_SHA" -f image/$ENV --build-arg PYTHON_VERS=$PYTHON_VERSION .
    - docker push "$CI_REGISTRY_IMAGE/$CHANNEL/$ENV:$CI_COMMIT_SHORT_SHA"
    - docker tag "$CI_REGISTRY_IMAGE/$CHANNEL/$ENV:$CI_COMMIT_SHORT_SHA" "$CI_REGISTRY_IMAGE/$CHANNEL/$ENV:latest"
    - docker push "$CI_REGISTRY_IMAGE/$CHANNEL/$ENV:latest"

.image:release:
  extends: .image
  variables:
    CHANNEL: "release"
  after_script:
    - docker tag "$CI_REGISTRY_IMAGE/$CHANNEL/$ENV:$CI_COMMIT_SHORT_SHA" "$CI_REGISTRY_IMAGE/$CHANNEL/$ENV:$CI_COMMIT_TAG"
    - docker push "$CI_REGISTRY_IMAGE/$CHANNEL/$ENV:$CI_COMMIT_TAG"
  rules:
    - if: $CI_COMMIT_TAG
      when: manual

image:release:poetry:
  extends: .image:release
  variables:
    ENV: "poetry"

image:release:pip:
  extends: .image:release
  variables:
    ENV: "pip"

.image:branch:
  extends: .image
  allow_failure: true
  variables:
    CHANNEL: $CI_COMMIT_BRANCH
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: manual

image:branch:poetry:
  extends: .image:branch
  variables:
    ENV: "poetry"

image:branch:pip:
  extends: .image:branch
  variables:
    ENV: "pip"

pypi_test:
  stage: package
  rules:
    - if: $CI_COMMIT_TAG
      when: manual
  script:
    - poetry config repositories.pypi_test https://test.pypi.org/legacy/
    - poetry publish --build --username $PYPI_TEST_LOGIN --password $PYPI_TEST_PASSWORD --repository pypi_test

pypi:
  stage: package
  rules:
    - if: $CI_COMMIT_TAG
      when: manual
  script:
    - poetry publish --build --username $PYPI_LOGIN --password $PYPI_PASSWORD

pages:
  extends: .changes
  needs: ["tests:3.9:coverage"]
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  stage: coverage
  script: mv cov_html/ public/
  artifacts:
    paths:
      - public
    expire_in: 2 days
