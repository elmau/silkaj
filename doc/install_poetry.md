## Install Silkaj in a development environement with Poetry

### Install libsodium

```bash
sudo apt install libsodium23 # Debian Buster
sudo apt install libsodium18 # Debian Stretch
sudo dnf install libsodium # Fedora
```

### Install Poetry

- [Installation documentation](https://python-poetry.org/docs/#installation)

### On Debian Buster

```bash
sudo apt install python3-pip python3-venv
sudo apt install libffi-dev # Required on ARMbian
pip3 install poetry --user
```

### Install dependencies and the Python virtual environment

```bash
# Over HTTPS
git clone https://git.duniter.org/clients/python/silkaj.git

# Over SSH
git clone git@git.duniter.org:clients/python/silkaj.git

cd silkaj

# Installs the runtime and development dependencies
poetry install
```

### Run Silkaj

Within `silkaj` repository run Silkaj:

```bash
poetry run silkaj
```

### Poetry shell

You can access tools (`pytest`, `black`) from within the development environment with `poetry run` or by entering the shell:

```bash
silkaj> poetry run pytest
silkaj> poetry run black
```

```bash
silkaj> poetry shell
(silkaj-58dUTebQ-py3.9) silkaj> black
(silkaj-58dUTebQ-py3.9) silkaj> pytest
```

### Make Silkaj accessible from everywhere

Add following alias to your shell configuration:

```bash
alias silkaj="cd /path/to/silkaj && poetry run silkaj"
```

### Contribute

Follow the [contributing guide](CONTRIBUTING.md).
