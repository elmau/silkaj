# Copyright  2016-2022 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

# import urllib
# from unittest.mock import patch

import pytest

# from duniterpy.api import bma
from duniterpy.api import endpoint as du_ep

from silkaj import constants, network_tools

# from silkaj.membership import generate_membership_document
from tests import helpers

ipv6 = "2001:0db8:85a3:0000:0000:8a2e:0370:7334"


@pytest.mark.parametrize(
    "endpoint, host, ipv4, ipv6, port, path",
    [
        ("127.0.0.1", "", "127.0.0.1", None, 443, None),
        ("127.0.0.1:80", "", "127.0.0.1", None, 80, None),
        ("127.0.0.1:443", "", "127.0.0.1", None, 443, None),
        ("127.0.0.1/path", "", "127.0.0.1", None, 443, "path"),
        ("127.0.0.1:80/path", "", "127.0.0.1", None, 80, "path"),
        ("domain.tld:80/path", "domain.tld", None, None, 80, "path"),
        ("localhost:80/path", "localhost", None, None, 80, "path"),
        (f"[{ipv6}]", None, None, ipv6, 443, None),
        (f"[{ipv6}]/path", None, None, ipv6, 443, "path"),
        (f"[{ipv6}]:80/path", None, None, ipv6, 80, "path"),
    ],
)
def test_determine_endpoint_custom(endpoint, host, ipv4, ipv6, port, path):
    helpers.define_click_context(endpoint)
    ep = network_tools.determine_endpoint()
    assert ep.host == host
    assert ep.ipv4 == ipv4
    assert ep.ipv6 == ipv6
    assert ep.port == port
    if isinstance(ep, du_ep.SecuredBMAEndpoint):
        assert ep.path == path


@pytest.mark.parametrize(
    "gtest, endpoint",
    [
        (True, constants.G1_TEST_DEFAULT_ENDPOINT),
        (False, constants.G1_DEFAULT_ENDPOINT),
    ],
)
def test_determine_endpoint(gtest, endpoint):
    helpers.define_click_context(gtest=gtest)
    ep = network_tools.determine_endpoint()
    assert ep == du_ep.endpoint(endpoint)


# def test_send_document_success(capsys):
#    display = capsys.readouterr().out
#    network_tools.send_document()


# def test_send_document_error(capsys):
#    membership_doc = generate_membership_document(
#        "g1", "A" * 43, "0-ahv", "test", "1-aut"
#    )
#
#    #with patch("urllib.request.urlopen", side_effect=urllib.error.HTTPError):
#    with patch("duniterpy.api.client.API.request_url", side_effect=urllib.error.HTTPError):
#        network_tools.send_document(bma.blockchain.membership, membership_doc)
#
#    display = capsys.readouterr().out
#    assert "Error while publishing Membership:" in display
