# Copyright  2016-2022 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

import pytest
from click.testing import CliRunner

from silkaj.constants import CENT_MULT_TO_UNIT
from silkaj.money import get_ud_value
from silkaj.tx import parse_file_containing_amounts_recipients

FILE_PATH = "recipients.txt"

ud_value = get_ud_value()


@pytest.mark.parametrize(
    "file_content, amounts_exp, recipients_exp",
    [
        (
            "ABSOLUTE\n10 pubkey1\n20 pubkey2",
            [10 * CENT_MULT_TO_UNIT, 20 * CENT_MULT_TO_UNIT],
            ["pubkey1", "pubkey2"],
        ),
        (
            "RELATIVE\n#toto\n10 pubkey1\n#titi\n20 pubkey2",
            [10 * ud_value, 20 * ud_value],
            ["pubkey1", "pubkey2"],
        ),
    ],
)
def test_parse_file_containing_amounts_recipients(
    file_content, amounts_exp, recipients_exp
):
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open(FILE_PATH, "w") as f:
            f.write(file_content)
        amounts, recipients = parse_file_containing_amounts_recipients(FILE_PATH)
    assert amounts == amounts_exp
    assert recipients == recipients_exp


HEADER_ERR = (
    "recipients.txt must contain at first line 'ABSOLUTE' or 'RELATIVE' header\n"
)
SYNTAX_ERR = "Syntax error at line"
SPEC_ERR = "No amounts or recipients specified"


@pytest.mark.parametrize(
    "file_content, error",
    [
        ("ABSOLUTE\n10 pubkey1\n20", SYNTAX_ERR),
        ("#RELATIVE\n10 pubkey1\n20 pubkey2", HEADER_ERR),
        ("RELATIVE\npubkey1 10\n20 pubkey2", SYNTAX_ERR),
        ("ABSOLUTE", SPEC_ERR),
        ("", HEADER_ERR),
    ],
)
def test_parse_file_containing_amounts_recipients_errors(file_content, error, capsys):
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open(FILE_PATH, "w") as f:
            f.write(file_content)
        with pytest.raises(SystemExit):
            parse_file_containing_amounts_recipients(FILE_PATH)
    assert error in capsys.readouterr().out
