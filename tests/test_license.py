# Copyright  2016-2022 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

from unittest.mock import patch

import pytest
from click.testing import CliRunner

from silkaj import cli, license
from silkaj.constants import SUCCESS_EXIT_STATUS


def test_license_approval_g1_test(capsys):
    license.license_approval("g1-test")
    assert not capsys.readouterr().out


# TODO: approve is not tested
@pytest.mark.parametrize(
    "display, approve",
    [
        (True, True),
        (True, False),
        (False, True),
        (False, False),
    ],
)
@patch("click.confirm")
@patch.object(license.G1MonetaryLicense, "display_license")
def test_license_approval_g1(mock_display_license, mock_confirm, display, approve):
    # https://stackoverflow.com/a/62939130
    mock_confirm.return_value = display
    license.license_approval("g1")
    if display:
        mock_display_license.assert_called_once()
    mock_confirm.assert_called()


@pytest.mark.parametrize(
    "language, license_sample",
    [
        ("en", "**Currency licensing"),
        ("", "**Currency licensing"),
        ("fr", "**Licence de la monnaie"),
        ("blabla", ""),
    ],
)
def test_language_prompt(language, license_sample):
    result = CliRunner().invoke(cli.cli, args=["license"], input=language)
    assert "In which language" in result.output
    assert license_sample in result.output
    assert result.exit_code == SUCCESS_EXIT_STATUS


def test_available_languages_and_get_license_path():
    g1ml = license.G1MonetaryLicense()
    for language_code in g1ml.languages_codes:
        assert g1ml.get_license_path(language_code).is_file()


def test_long_language_code_handling():
    language_code = "fr-FR"
    content = "Licence monétaire Ğ1"
    g1ml = license.G1MonetaryLicense()
    license_path = g1ml.get_license_path(language_code)
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open(license_path, "w") as f:
            f.write(content)
        result = runner.invoke(cli.cli, args=["license"], input=language_code)
        assert language_code in result.output
        assert content in result.output
    license_path.unlink()
