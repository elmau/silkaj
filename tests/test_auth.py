# Copyright  2016-2022 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

import click
import pytest

from patched.auth import (
    patched_auth_by_auth_file,
    patched_auth_by_scrypt,
    patched_auth_by_seed,
    patched_auth_by_wif,
)
from silkaj import auth


# test auth_method
@pytest.mark.parametrize(
    "seed, file, wif, auth_method_called",
    [
        (True, False, False, "call_auth_by_seed"),
        (False, True, False, "call_auth_by_auth_file"),
        (False, False, True, "call_auth_by_wif"),
        (False, False, False, "call_auth_by_scrypt"),
    ],
)
def test_auth_method(seed, file, wif, auth_method_called, monkeypatch):
    monkeypatch.setattr("silkaj.auth.auth_by_seed", patched_auth_by_seed)
    monkeypatch.setattr("silkaj.auth.auth_by_wif", patched_auth_by_wif)
    monkeypatch.setattr("silkaj.auth.auth_by_auth_file", patched_auth_by_auth_file)
    monkeypatch.setattr("silkaj.auth.auth_by_scrypt", patched_auth_by_scrypt)
    ctx = click.Context(
        click.Command(""), obj={"AUTH_SEED": seed, "AUTH_FILE": file, "AUTH_WIF": wif}
    )
    with ctx:
        assert auth_method_called == auth.auth_method()
