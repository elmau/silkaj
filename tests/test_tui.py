# Copyright  2016-2022 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

import pytest

from patched.test_constants import mock_ud_value
from patched.wot import patched_is_member
from silkaj import wot_tools
from silkaj.constants import G1_SYMBOL, SHORT_PUBKEY_SIZE
from silkaj.tui import display_amount, display_pubkey, gen_pubkey_checksum


# display_amount()
@pytest.mark.parametrize(
    "message, amount, currency_symbol", [("Total", 1000, G1_SYMBOL)]
)
def test_display_amount(message, amount, currency_symbol):
    amount_UD = round(amount / mock_ud_value, 2)
    expected = [
        [
            f"{message} (unit|relative)",
            f"{str(amount / 100)} {currency_symbol} | {str(amount_UD)} UD {currency_symbol}",
        ]
    ]
    tx = list()
    display_amount(tx, message, amount, mock_ud_value, currency_symbol)
    assert tx == expected


# display_pubkey()
@pytest.mark.parametrize(
    "message, pubkey, id",
    [
        ("From", "CtM5RZHopnSRAAoWNgTWrUhDEmspcCAxn6fuCEWDWudp", "riri"),
        ("To", "DBM6F5ChMJzpmkUdL5zD9UXKExmZGfQ1AgPDQy4MxSBw", ""),
    ],
)
def test_display_pubkey(message, pubkey, id, monkeypatch):
    monkeypatch.setattr(wot_tools, "is_member", patched_is_member)

    expected = [[f"{message} (pubkey:checksum)", gen_pubkey_checksum(pubkey)]]
    if id:
        expected.append([f"{message} (id)", id])
    tx = list()
    display_pubkey(tx, message, pubkey)
    assert tx == expected


# gen_pubkey_checksum
@pytest.mark.parametrize(
    "pubkey, checksum",
    [
        ("J4c8CARmP9vAFNGtHRuzx14zvxojyRWHW2darguVqjtX", "KAv"),
    ],
)
def test_gen_pubkey_checksum(pubkey, checksum):
    assert pubkey + ":" + checksum == gen_pubkey_checksum(pubkey)
    assert pubkey[:SHORT_PUBKEY_SIZE] + "…:" + checksum == gen_pubkey_checksum(
        pubkey, short=True
    )
    assert pubkey[:14] + "…:" + checksum == gen_pubkey_checksum(
        pubkey, short=True, length=14
    )
