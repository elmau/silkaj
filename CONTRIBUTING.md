# Contributing

## Goals

Part of the Duniter project running the Ğ1 currency, Silkaj project is aiming to create a generic tool to manage his account and wallets, and to monitor the currency.

## Install the development environment

We are using [Poetry](https://python-poetry.org/) as a development environment solution. Start [installing Poetry](doc/install_poetry.md).
This will install a sandboxed Python environment.
Dependencies will be installed in it in order to have Silkaj running and to have pre-installed developement tools.

## Workflow

- We use branches for merge requests
- We prefer fast-forward and rebase method than having a merge commit. This in order to have a clean history.

## Branches

- `main`: development and stable branch
- maintainance branches, to maintain a stable version while developing future version with breaking changes. For instance: `0.10`

## Developing with DuniterPy

[DuniterPy](https://git.duniter.org/clients/python/duniterpy) is a Python library for Duniter clients.
It implements a client with multiple APIs, the handling for document signing.
As it is coupled with Silkaj, it is oftenly needed to develop in both repositories.

### How to use DuniterPy as editable with Poetry

Clone DuniterPy locally alongside of `silkaj` repository:

```bash
silkaj> cd ..
git clone https://git.duniter.org/clients/python/duniterpy
```

Use DuniterPy as a [path dependency](https://python-poetry.org/docs/dependency-specification/#path-dependencies):

```bash
poetry add ../duniterpy
```

### Developing with modules

Silkaj is using Python modules which shape kind of a framework.
Please read their documentations on how to use them the best possible.

- [DuniterPy](https://clients.duniter.io/python/duniterpy/index.html): Autogenerated documentation.
  - Feel free to contribute upstream to share the code with other Python programs
- [Click](https://click.palletsprojects.com/#documentation)
- [Pendulum](https://pendulum.eustace.io/docs/)
- [texttable](https://github.com/foutaise/texttable/#documentation)

## Pre-commit

We are using [`pre-commit`](https://pre-commit.com/) tool to perform checks on staged changes before committing.
We are using it for `black` formatting, `isort` imports sorting, `pyupgrade`, and `gitlab-ci` linting.
We will use it for `pylint` code linting, `mypy` typing in the future.

Install `pre-commit` from your distribution. In case it is an outdated version, install it from `pip`:

```bash
sudo apt install pre-commit
pip install --user pre-commit
```

To install the `git-hooks`, run:

```bash
> pre-commit install
```

Then each time you commit changes, the hooks will perform checks.

To manually run one of the tool above, run (eg for `isort`):

```bash
> pre-commit run --all-files isort
```

To run all checks on all files:

```bash
> pre-commit run -a
```

### Authorization for GitLab CI linter hook

`pre-commit run -a (gitlab-ci-linter)` is failing due to authorization required for CI lint API accesses.
When running this command, just ignore this failed hook.
In case you want to commit a `.gitlab-ci.yml` edition, this hook will prevent the commit creation.
You can [skip the hooks](https://stackoverflow.com/a/7230886) with `git commit -m "msg" --no-verify`.
This is fine for occasional `.gitlab-ci.yml` editions. In case you would like to edit this file more often and have it checked, ask a maintainer to provide you with `GITLAB_PRIVATE_TOKEN` environment variable that can be set into a shell configuration.
With Bash, in `$HOME/.bashrc` add the following:

```bash
export GITLAB_PRIVATE_TOKEN=""
```

With Fish, in `$HOME/.config/fish/config.fish` add the following:

```fish
set -xg GITLAB_PRIVATE_TOKEN ""
```

Check out duniterpy#169 for more details.

### Black formatting

We are using [Black](https://github.com/psf/black) formatter tool.
Run Black on a Python file to format it:

```bash
poetry run black silkaj/cli.py
```

With `pre-commit`, Black is called on staged files, so the commit should fail in case black would make changes.
You will have to `git add` Black changes in order to commit your changes.

## Tests

We are using [Pytest](https://pytest.org) as a tests framework. To know more about how Silkaj implement it read the [project test documentation](doc/test_and_coverage.md).

To run tests, within `silkaj` repository:

```bash
poetry run pytest
```

### How to test a single file

Specifiy the path of the test:

```bash
poetry run pytest tests/test_end_to_end.py
```

## Version update

We are using the [Semantic Versioning](https://semver.org).

To create a release, we use following script which will update the version in different files, and will make a commit and a tag out of it.

```bash
./release.sh 0.8.1
```

Then, a `git push --tags` is necessary to publish the tag. Git could be configured to publish tags with a simple `push`.

### How to release a pre-release on PyPI

[Append `[{a|b|rc}N]` to the version, it will be automatically detected as pre-release by PyPI](https://pythonpackaging.info/07-Package-Release.html). i.e.: `v0.10.0rc0`.

- install a pre-release from PyPI:

```sh
pip install silkaj --user --upgrade --pre
```

- install `silkaj` from PyPI test and the dependencies (i.e. DuniterPy) from PyPI (have been removed from the documentation):

```sh
pip install silkaj --user --upgrade -i https://test.pypi.org/simple/ --extra-index-url https://pypi.org/simple/
```

### Update copyright year

Follow [this documentation](https://github.com/Lucas-C/pre-commit-hooks#removing-old-license-and-replacing-it-with-a-new-one)
Only difference is to update the year in `license_header.txt` rather than `LICENSE.txt`.

## PyPI and PyPI test distributions

Silkaj is distributed to PyPI, the Python Package Index, for further `pip` installation.
Silkaj can be published to [PyPI](https://pypi.org/project/silkaj) or to [PyPI test](https://test.pypi.org/project/silkaj/) for testing purposes.
Publishing to PyPI or PyPI test can be directly done from the continuous delivery or from Poetry it-self.
The CD jobs does appear on a tag and have to be triggered manually.
Only the project maintainers have the rights to publish tags.

### PyPI

Publishing to PyPI from Poetry:

```bash
poetry publish --build
```

### PyPI test

Publishing to PyPI test from Poetry:

```bash
poetry config repositories.pypi_test https://test.pypi.org/legacy/
poetry publish --build --repository pypi_test
```

To install this package:

```bash
pip install --index-url https://test.pypi.org/simple/ --extra-index-url https://pypi.python.org/simple/ silkaj
```

The `--extra-index-url` is used to retrieve dependencies packages from the official PyPI not to get issues with missing or testing dependencies comming from PyPI test repositories.

## Continuous integration and delivery

### Own built Docker images

- https://git.duniter.org/docker/python3/poetry
- Python images based on Debian Buster
- Poetry installed on top
- Black installed on v3.8

### Jobs

- Checks:
  - Format
  - Build
- Tests on supported Python versions:
  - Installation
  - Pytest on Python supported versions
- PyPI distribution
  - test
  - stable

### Ğ1 monetary license update

To modify the Ğ1 monetary license files, please change them on [its repository](https://git.duniter.org/documents/g1_monetary_license), since it’s integrated in silkaj repository as a `git subtree`.

To pull changes from upstream with `git subtree`, add the repository as a remote then pull:

```bash
git remote add g1_monetary_license https://git.duniter.org/documents/g1_monetary_license.git
git subtree pull --prefix g1_monetary_license g1_monetary_license master
```
