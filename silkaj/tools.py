# Copyright  2016-2022 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

import functools
from sys import exit

from silkaj.blockchain_tools import get_blockchain_parameters
from silkaj.constants import FAILURE_EXIT_STATUS, G1_SYMBOL, GTEST_SYMBOL


@functools.lru_cache(maxsize=1)
def get_currency_symbol() -> str:
    params = get_blockchain_parameters()
    if params["currency"] == "g1":
        return G1_SYMBOL
    return GTEST_SYMBOL


def message_exit(message: str) -> None:
    print(message)
    exit(FAILURE_EXIT_STATUS)
