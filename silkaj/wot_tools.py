# Copyright  2016-2022 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

from typing import Dict, List, Optional
from urllib.error import HTTPError

from duniterpy.api.bma import wot

from silkaj.network_tools import client_instance


def identity_of(pubkey_uid: str) -> Dict:
    """
    Only works for members
    Not able to get corresponding uid from a non-member identity
    Able to know if an identity is member or not
    """
    client = client_instance()
    return client(wot.identity_of, pubkey_uid)


def is_member(pubkey_uid: str) -> Optional[Dict]:
    """
    Check identity is member
    If member, return corresponding identity, else: False
    """
    try:
        return identity_of(pubkey_uid)
    except HTTPError:
        return None


def wot_lookup(identifier: str) -> List:
    """
    :identifier: identity or pubkey in part or whole
    Return received and sent certifications lists of matching identities
    if one identity found
    """
    client = client_instance()
    return (client(wot.lookup, identifier))["results"]


def identities_from_pubkeys(pubkeys: List[str], uids: bool) -> List:
    """
    Make list of pubkeys unique, and remove empty strings
    Request identities
    """
    if not uids:
        return list()

    uniq_pubkeys = list(filter(None, set(pubkeys)))
    identities = list()
    for pubkey in uniq_pubkeys:
        try:
            identities.append(identity_of(pubkey))
        except HTTPError:
            pass
    return identities
