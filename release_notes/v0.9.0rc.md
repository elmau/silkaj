## Call for testing Silkaj v0.9.0rc

Hello everyone!

We are pleased to publish a release candidate of Silkaj v0.9.0, and we would be happy to receive feedback before releasing it as a stable version.

To install this pre-release version from PyPI, use this command:

```sh
pip3 install silkaj --user --upgrade --pre
```

Please test it globally, and check [the changelog](https://git.duniter.org/clients/python/silkaj/-/blob/main/CHANGELOG.md#v090rc-24th-march-2021) which contains the changes which happen during this development cycle.
Pay a special attention to the transaction part where a part of the algorithm changed.
There are new options on following commands:

```sh
silkaj history --full-pubkey
silkaj --dry-run cert
silkaj --dry-run/--display membership
```

Dry-run and display options are defined as general options, and only defined for this three cases for this release.

# Tests

Manual tests help us making sure everything works fine for different cases.
Here are some tests we think are necessary.
Make sure you test on Ğ1-Test network to avoid any loss of money.

## `tx`

- send a TX to a unique recipient
- send a TX to multiple recipients
  - with one amount
  - with multiple amounts
- send a TX to 92 recipients (can be 92 times the same)
- send a TX to 93 recipients (should fail)

## `membership`

- renew membership with `--dry-run` global option
- renew membership with `--display` global option
- renew membership without these two options

## `cert`

- send a certification with `--display` global option
- cert all identities you can on GTest network (thanks for keeping it alive ;-) )
- cert pubkey `4KEA63RCFF7AXUePPg5Q7JX9RtzXjywai1iKmE7LcoEC:DRz` on Ğ1-Test -> you should be suggested two identities
- cert identity `ggg_ggg_2` on Ğ1-Test -> you should NOT be proposed many identities

## `history`

- check you history
- check you history and display userIDs
- check you history and display pubkeys in full-length
- check you history and display userIDs and pubkeys in full-length

## `wot`

- check your WoT infos are correct with `wot` command

## `checksum`

- use `checksum` command to compute a checksum for one of your public keys.
- verify it with Silkaj
- verify it with Cesium
- try to change a character in the public key (with the checksum), then verify that it is wrong.

## auth

- create an authfile for a Ğ1-Test account
- send txs or certs using the authfile

## Other

Feel free to play!

______________________________________________________________________

The release is planned for the 17th April of 2021, which will contain a detailed announcement of the changes and the new features.

Silkaj team
